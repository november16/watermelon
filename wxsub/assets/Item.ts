// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Item extends cc.Component {

    @property(cc.Label)
    nickName: cc.Label = null;

    @property(cc.Label)
    huoguoNum: cc.Label = null;

    @property(cc.Label)
    scoreNum: cc.Label = null;

    @property(cc.Node)
    rankHead: cc.Node = null;

    public InitItem(avatarUrl:string,nickName:string, huoguoNum:string,scoreNum:string)
    {
        var _this = this;
        cc.loader.load({url: avatarUrl, type: 'png'},function(err,texture){
            _this.rankHead.getComponent(cc.Sprite).spriteFrame=new cc.SpriteFrame(texture);
        });
        this.nickName.string = nickName;
        this.huoguoNum.string = huoguoNum;
        this.scoreNum.string = scoreNum;
    }
 
}
