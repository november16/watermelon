# Watermelon

#### 介绍
合成大西瓜

#### 软件架构
Watermelon文件夹是Cocos creator项目，
主要是发布微信小游戏
作为主域
wxsub文件是Cocos creator项目，
作为子域
实现微信小游戏排行榜功能排行榜功能


#### 扫码体验
![合成大西瓜](https://images.gitee.com/uploads/images/2021/0316/102007_5f1e7501_890859.jpeg "gh_3a13cf22e37a_258.jpg")


#### 更多分享
![公众号](https://images.gitee.com/uploads/images/2021/0316/102227_7ba02e14_890859.png "扫码_搜索联合传播样式-标准色版.png")


#### 参与贡献

郑州橙卡软件科技有限公司
http://vr.orangecard.cn/


#### 特技

长期承接各类游戏开发，虚拟仿真程序，VR，AR应用开发


