

const { ccclass, property } = cc._decorator;

@ccclass
export default class AudioMgr extends cc.Component {

    public static instance: AudioMgr;
   //背景音效
   @property(cc.AudioSource)
   bgSource: cc.AudioSource = null;
   //合成音效
   @property(cc.AudioSource)
   effectSource: cc.AudioSource = null;
   //播放合成音效
   public PlayEffectSource() {
       this.effectSource.play();
   }

    start() {
        AudioMgr.instance = this;
    }


}
