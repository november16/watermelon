import Player from "./Player";
import AudioMgr from "./AudioMgr";


const { ccclass, property } = cc._decorator;

@ccclass
export default class Fruit extends cc.Component {

    public isBValid:boolean = false;
    public radius: number = 0;
    onLoad() {
        this.radius = this.node.getComponent(cc.PhysicsCircleCollider).radius;
    }
    state: boolean = false;
    onBeginContact(contact, self: cc.PhysicsCollider, other: cc.PhysicsCollider) {

        if(this.isBValid == false && other.name.startsWith("Wall") == false)
        {
            this.isBValid = true;
        }
        if (self.tag == other.tag) {
            this.state = true;
            if (self.tag < 11) {
                self.node.destroy();
                if (other.node.getComponent(Fruit).state == false) {
                    Player.instance.CreatBiggerFruit(new cc.Vec2(this.node.position.x, this.node.position.y), self.tag);
                    Player.instance.score += self.tag;
                    AudioMgr.instance.PlayEffectSource();
                    if (self.tag == 10) {
                        Player.instance.watermellonNum += 1;
                    }
                }
            }
        }
    }   
}
